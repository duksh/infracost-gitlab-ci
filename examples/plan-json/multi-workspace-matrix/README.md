# Multi-Terraform workspace with matrix jobs

This example shows how to run Infracost in GitLab CI against a Terraform project that uses multiple workspaces using parallel matrix jobs. The first job uses a matrix to generate the plan JSONs and the second job uses another matrix to generate multiple Infracost output JSON files. `infracost comment` command in the last job combines the Infracost JSON files and posts a single comment.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> This test is skipped locally since the local gitlab-ci-local tool doesn't yet support matrix builds
[//]: <> See https://github.com/firecow/gitlab-ci-local/issues/205
[//]: <> (BEGIN EXAMPLE: plan-json-multi-workspace-matrix)
```yml
variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/plan-json/multi-workspace-matrix/code
stages:
  - plan
  - infracost_diff
  - infracost_comment

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform

plan:
  stage: plan
  image:
    name: hashicorp/terraform:latest
    entrypoint: [""]
  before_script:
    - cd ${TF_ROOT}
    - terraform init
  script:
    - terraform plan -out=${TF_WORKSPACE}-plan.cache -var-file=${TF_WORKSPACE}.tfvars
    - terraform show -json ${TF_WORKSPACE}-plan.cache > ${TF_WORKSPACE}-plan.json
  artifacts:
    paths:
      - ${TF_ROOT}/${TF_WORKSPACE}-plan.cache
      - ${TF_ROOT}/${TF_WORKSPACE}-plan.json
  parallel:
    matrix:
        # This env variable is also used by terraform to select the workspace
      - TF_WORKSPACE: dev
        # IMPORTANT: add any cloud credentials as masked variables so Terraform can run
        AWS_ACCESS_KEY_ID: $DEV_AWS_ACCESS_KEY_ID
        AWS_SECRET_ACCESS_KEY: $DEV_SECRET_AWS_ACCESS_KEY
      - TF_WORKSPACE: prod
        AWS_ACCESS_KEY_ID: $PROD_AWS_ACCESS_KEY_ID
        AWS_SECRET_ACCESS_KEY: $PROD_SECRET_AWS_ACCESS_KEY
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

infracost_diff:
  stage: infracost_diff
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  dependencies:
    - plan
  script:
    # Generate an Infracost diff and save it to a JSON file.
    - |
      infracost diff --path=${TF_ROOT}/${TF_WORKSPACE}-plan.json \
                     --format=json \
                     --out-file=infracost_${TF_WORKSPACE}.json
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
  artifacts:
    paths:
      - infracost_${TF_WORKSPACE}.json
  parallel:
    matrix:
      - TF_WORKSPACE:
        - dev
        - prod
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

infracost_comment:
  stage: infracost_comment
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  dependencies:
    - infracost_diff
  script:
    # Posts a comment to the PR using the 'update' behavior.
    # This creates a single comment and updates it. The "quietest" option.
    # The other valid behaviors are:
    #   delete-and-new - Delete previous comments and create a new one.
    #   new - Create a new cost estimate comment on every push.
    # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options.
    - |
      infracost comment gitlab --path="infracost_*.json" \
                               --repo=$CI_PROJECT_PATH \
                               --merge-request=$CI_MERGE_REQUEST_IID \
                               --gitlab-server-url=$CI_SERVER_URL \
                               --gitlab-token=$GITLAB_TOKEN \
                               --behavior=update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
