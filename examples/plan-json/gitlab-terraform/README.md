# GitLab Terraform

This example shows how to run Infracost on GitLab CI with the GitLab Terraform image.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: plan-json-gitlab-terraform)
```yml
include:
  - template: Terraform/Base.latest.gitlab-ci.yml

variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/plan-json/gitlab-terraform/code

stages:
  - build
  - plan_json
  - infracost

build:
  extends: .terraform:build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

plan_json:
  stage: plan_json
  dependencies:
    - build
  before_script:
    # IMPORTANT: add any required steps here to setup cloud credentials so Terraform can run
    - cd ${TF_ROOT}
    - gitlab-terraform init
  script:
    - gitlab-terraform show -json plan.cache > full-plan.json
  artifacts:
    paths:
      - ${TF_ROOT}/full-plan.json
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

infracost:
  stage: infracost
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  dependencies:
    - plan_json
  script:
    # Generate an Infracost diff and save it to a JSON file.
    - |
      infracost diff --path=${TF_ROOT}/full-plan.json \
                     --format=json \
                     --out-file=infracost.json

    # Posts a comment to the PR using the 'update' behavior.
    # This creates a single comment and updates it. The "quietest" option.
    # The other valid behaviors are:
    #   delete-and-new - Delete previous comments and create a new one.
    #   new - Create a new cost estimate comment on every push.
    # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options.
    - |
      infracost comment gitlab --path=infracost.json \
                               --repo=$CI_PROJECT_PATH \
                               --merge-request=$CI_MERGE_REQUEST_IID \
                               --gitlab-server-url=$CI_SERVER_URL \
                               --gitlab-token=$GITLAB_TOKEN \
                               --behavior=update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
